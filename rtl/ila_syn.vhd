library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;


entity ila is

    generic (
        ila_en         : boolean := true );
    port (
        clk0           : in  std_logic;
        ila0           : in  std_logic_vector(255 downto 0) );
        
  attribute keep_hierarchy : STRING;
  attribute keep_hierarchy of ila : entity is "yes";    

end ila;

architecture rtl of ila is
    
    -- misc
    signal tied_to_gnd  : std_logic := '0';

    -- debug i/f
    signal ila0_r        : std_logic_vector(255 downto 0);
    signal ila0_h        : std_logic_vector(255 downto 0);
    
    
    attribute shreg_extract           : string;    
    attribute shreg_extract of ila0_r : signal is "no";
    attribute shreg_extract of ila0_h : signal is "no";
    
    signal icon_control0 : std_logic_vector(35 downto 0);

    component chipscope_icon
        port (
            control0	: inout std_logic_vector(35 downto 0) );
    end component;
    
    component chipscope_ila
        port (
            control	: inout std_logic_vector(35 downto 0);
            clk	        : in    std_logic;
            trig0	: in    std_logic_vector(255 downto 0) );
    end component;
    
begin

    ila_g : if ila_en = true generate
        
        icon_i : chipscope_icon
            port map(
                control0 => icon_control0 );
        
        ila_i : chipscope_ila
            port map (
            control => icon_control0,
            clk     => clk0,
            trig0   => ila0_h );
        
    end generate;
        
    process (clk0) is
    begin        
        if rising_edge(clk0) then
            
            ila0_r <= ila0;
            ila0_h <= ila0_r;
                
        end if;
    end process;
    
        
end architecture;
