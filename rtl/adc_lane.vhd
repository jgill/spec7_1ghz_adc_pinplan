library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity adc_lane is

   port (
      adc_clk_bufio_i : in  std_logic;
      adc_clk_bufr_i  : in  std_logic;

      rst_i           : in  std_logic;
      bitslip_i       : in  std_logic;
      
      adc_data_p_i    : in  std_logic;
      adc_data_n_i    : in  std_logic;
      
      adc_data_o      : out std_logic_vector(7 downto 0) );
   
end entity;

architecture rtl of adc_lane is

   signal adc_data_in : std_logic;

   signal adc_clk_bufio_n : std_logic;
   
begin

  adc_clk_bufio_n <= not adc_clk_bufio_i;
  
   ibuf_1 : IBUFDS
      generic map (
         IOSTANDARD => "LVDS")
      port map (
         I  => adc_data_p_i,
         IB => adc_data_n_i,
         O  => adc_data_in );
   
   
   inst_ISERDESE2 : ISERDESE2
      generic map (
         DATA_RATE => "DDR",              -- DDR, SDR
         DATA_WIDTH => 8,                 -- Parallel data width (2-8,10,14)
         DYN_CLKDIV_INV_EN => "FALSE",    -- Enable DYNCLKDIVINVSEL inversion (FALSE, TRUE)
         DYN_CLK_INV_EN => "FALSE",       -- Enable DYNCLKINVSEL inversion (FALSE, TRUE)
         INIT_Q1 => '0',                  -- INIT_Q1 - INIT_Q4: Initial value on the Q outputs (0/1)
         INIT_Q2 => '0',
         INIT_Q3 => '0',
         INIT_Q4 => '0',
         INTERFACE_TYPE => "NETWORKING",  -- MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
         IOBDELAY => "NONE",               -- NONE, BOTH, IBUF, IFD
         NUM_CE => 2,                     -- Number of clock enables (1,2)
         OFB_USED => "FALSE",             -- Select OFB path (FALSE, TRUE)
         SERDES_MODE => "MASTER",         -- MASTER, SLAVE
         SRVAL_Q1 => '0',                 -- SRVAL_Q1 - SRVAL_Q4: Q output values when SR is used (0/1)
         SRVAL_Q2 => '0',
         SRVAL_Q3 => '0',
         SRVAL_Q4 => '0'
         )
      port map (
         O => open,                       -- 1-bit output: Combinatorial output
         Q1 => adc_data_o(0),           -- Q1 - Q8: 1-bit (each) output: Registered data outputs
         Q2 => adc_data_o(1),           -- FIXME: check order
         Q3 => adc_data_o(2),
         Q4 => adc_data_o(3),
         Q5 => adc_data_o(4),
         Q6 => adc_data_o(5),
         Q7 => adc_data_o(6),
         Q8 => adc_data_o(7),
         SHIFTOUT1 => open,               -- SHIFTOUT1, SHIFTOUT2: 1-bit (each) output: Data width expansion output ports
         SHIFTOUT2 => open,
         BITSLIP => bitslip_i,                  -- The BITSLIP pin performs a Bitslip operation synchronous
         CE1 => '1',                      -- CE1, CE2: 1-bit (each) input: Data register clock enable inputs
         CE2 => '1',
         CLKDIVP => '0',                  -- 1-bit input: TBD
         CLK => adc_clk_bufio_i,                 -- Clocks: 1-bit (each) input: ISERDESE2 clock input ports
         CLKB => adc_clk_bufio_n,
         CLKDIV => adc_clk_bufr_i,             -- 1-bit input: Divided clock
         OCLK => '0',                     -- 1-bit input: High speed output clock used when INTERFACE_TYPE="MEMORY"
         DYNCLKDIVSEL => '0',             -- 1-bit input: Dynamic CLKDIV inversion
         DYNCLKSEL => '0',                -- 1-bit input: Dynamic CLK/CLKB inversion
         D => adc_data_in,                        -- 1-bit input: Data input
         DDLY => '0',                     -- 1-bit input: Serial data from IDELAYE2
         OFB => '0',                      -- 1-bit input: Data feedback from OSERDESE2
         OCLKB => '0',                    -- 1-bit input: High speed negative edge output clock
         RST => rst_i,                  -- 1-bit input: Active high asynchronous reset
         SHIFTIN1 => '0',                 -- SHIFTIN1, SHIFTIN2: 1-bit (each) input: Data width expansion input ports
         SHIFTIN2 => '0' );
   
end architecture;
