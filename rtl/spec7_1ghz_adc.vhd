library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity spec7_1ghz_adc is

  port (
    fmc0_la_p_b : in std_logic_vector(33 downto 0);
    fmc0_la_n_b : in std_logic_vector(33 downto 0);
    fmc1_la_p_b : in std_logic_vector(33 downto 0);
    fmc1_la_n_b : in std_logic_vector(33 downto 0));

  attribute shreg_extract                   : string;
  attribute shreg_extract of spec7_1ghz_adc : entity is "no";

end entity;

architecture rtl of spec7_1ghz_adc is

  constant d8width : integer := 8;
  subtype d8_t is std_logic_vector(d8width-1 downto 0);
  type v8_t is array (natural range <>) of d8_t;
  type v8_2d_t is array (natural range <>, natural range <>) of d8_t;

  signal adc_data : v8_2d_t(3 downto 0, 7 downto 0);

  signal adc_lclk      : std_logic_vector(3 downto 0);
  signal adc_clk_bufio : std_logic_vector(3 downto 0);
  signal adc_clk_bufr  : std_logic_vector(3 downto 0);

  signal ila_sigs : std_logic_vector(255 downto 0);
  signal ila_clk  : std_logic;

  signal serdes_auto_bslip : std_logic_vector(3 downto 0);
  signal bitslip_sreg      : v8_t(3 downto 0);

  signal adc_lclk_p    : std_logic_vector(3 downto 0);
  signal adc_lclk_n    : std_logic_vector(3 downto 0);
  signal adc_fclk_p    : std_logic_vector(3 downto 0);
  signal adc_fclk_n    : std_logic_vector(3 downto 0);
  signal adc_data_in_p : v8_t(3 downto 0);
  signal adc_data_in_n : v8_t(3 downto 0);
  signal adc_fclk      : v8_t(3 downto 0);

  -- signal mmcm_clkout0 : std_logic_vector(3 downto 0);
  -- signal mmcm_clkout1 : std_logic_vector(3 downto 0);
  -- signal mmcm_fbout   : std_logic_vector(3 downto 0);
  -- signal mmcm_fbin    : std_logic_vector(3 downto 0);

begin

  adc_lclk_p(0) <= fmc0_la_p_b(0);
  adc_lclk_n(0) <= fmc0_la_n_b(0);
  adc_fclk_p(0) <= fmc0_la_p_b(2);
  adc_fclk_n(0) <= fmc0_la_n_b(2);

  adc_lclk_p(1) <= fmc0_la_p_b(17);
  adc_lclk_n(1) <= fmc0_la_n_b(17);
  adc_fclk_p(1) <= fmc0_la_p_b(19);
  adc_fclk_n(1) <= fmc0_la_n_b(19);

  adc_lclk_p(2) <= fmc1_la_p_b(0);
  adc_lclk_n(2) <= fmc1_la_n_b(0);
  adc_fclk_p(2) <= fmc1_la_p_b(2);
  adc_fclk_n(2) <= fmc1_la_n_b(2);

  adc_lclk_p(3) <= fmc1_la_p_b(17);
  adc_lclk_n(3) <= fmc1_la_n_b(17);
  adc_fclk_p(3) <= fmc1_la_p_b(19);
  adc_fclk_n(3) <= fmc1_la_n_b(19);

  adc_data_in_p(0)(0) <= fmc0_la_p_b(3);
  adc_data_in_n(0)(0) <= fmc0_la_n_b(3);
  adc_data_in_p(0)(1) <= fmc0_la_p_b(4);
  adc_data_in_n(0)(1) <= fmc0_la_n_b(4);
  adc_data_in_p(0)(2) <= fmc0_la_p_b(5);
  adc_data_in_n(0)(2) <= fmc0_la_n_b(5);
  adc_data_in_p(0)(3) <= fmc0_la_p_b(6);
  adc_data_in_n(0)(3) <= fmc0_la_n_b(6);
  adc_data_in_p(0)(4) <= fmc0_la_p_b(7);
  adc_data_in_n(0)(4) <= fmc0_la_n_b(7);
  adc_data_in_p(0)(5) <= fmc0_la_p_b(8);
  adc_data_in_n(0)(5) <= fmc0_la_n_b(8);
  adc_data_in_p(0)(6) <= fmc0_la_p_b(9);
  adc_data_in_n(0)(6) <= fmc0_la_n_b(9);
  adc_data_in_p(0)(7) <= fmc0_la_p_b(10);
  adc_data_in_n(0)(7) <= fmc0_la_n_b(10);

  adc_data_in_p(1)(0) <= fmc0_la_p_b(20);
  adc_data_in_n(1)(0) <= fmc0_la_n_b(20);
  adc_data_in_p(1)(1) <= fmc0_la_p_b(21);
  adc_data_in_n(1)(1) <= fmc0_la_n_b(21);
  adc_data_in_p(1)(2) <= fmc0_la_p_b(22);
  adc_data_in_n(1)(2) <= fmc0_la_n_b(22);
  adc_data_in_p(1)(3) <= fmc0_la_p_b(23);
  adc_data_in_n(1)(3) <= fmc0_la_n_b(23);
  adc_data_in_p(1)(4) <= fmc0_la_p_b(24);
  adc_data_in_n(1)(4) <= fmc0_la_n_b(24);
  adc_data_in_p(1)(5) <= fmc0_la_p_b(25);
  adc_data_in_n(1)(5) <= fmc0_la_n_b(25);
  adc_data_in_p(1)(6) <= fmc0_la_p_b(26);
  adc_data_in_n(1)(6) <= fmc0_la_n_b(26);
  adc_data_in_p(1)(7) <= fmc0_la_p_b(27);
  adc_data_in_n(1)(7) <= fmc0_la_n_b(27);

  adc_data_in_p(2)(0) <= fmc1_la_p_b(3);
  adc_data_in_n(2)(0) <= fmc1_la_n_b(3);
  adc_data_in_p(2)(1) <= fmc1_la_p_b(4);
  adc_data_in_n(2)(1) <= fmc1_la_n_b(4);
  adc_data_in_p(2)(2) <= fmc1_la_p_b(5);
  adc_data_in_n(2)(2) <= fmc1_la_n_b(5);
  adc_data_in_p(2)(3) <= fmc1_la_p_b(6);
  adc_data_in_n(2)(3) <= fmc1_la_n_b(6);
  adc_data_in_p(2)(4) <= fmc1_la_p_b(7);
  adc_data_in_n(2)(4) <= fmc1_la_n_b(7);
  adc_data_in_p(2)(5) <= fmc1_la_p_b(8);
  adc_data_in_n(2)(5) <= fmc1_la_n_b(8);
  adc_data_in_p(2)(6) <= fmc1_la_p_b(9);
  adc_data_in_n(2)(6) <= fmc1_la_n_b(9);
  adc_data_in_p(2)(7) <= fmc1_la_p_b(10);
  adc_data_in_n(2)(7) <= fmc1_la_n_b(10);

  adc_data_in_p(3)(0) <= fmc1_la_p_b(20);
  adc_data_in_n(3)(0) <= fmc1_la_n_b(20);
  adc_data_in_p(3)(1) <= fmc1_la_p_b(21);
  adc_data_in_n(3)(1) <= fmc1_la_n_b(21);
  adc_data_in_p(3)(2) <= fmc1_la_p_b(22);
  adc_data_in_n(3)(2) <= fmc1_la_n_b(22);
  adc_data_in_p(3)(3) <= fmc1_la_p_b(23);
  adc_data_in_n(3)(3) <= fmc1_la_n_b(23);
  adc_data_in_p(3)(4) <= fmc1_la_p_b(24);
  adc_data_in_n(3)(4) <= fmc1_la_n_b(24);
  adc_data_in_p(3)(5) <= fmc1_la_p_b(25);
  adc_data_in_n(3)(5) <= fmc1_la_n_b(25);
  adc_data_in_p(3)(6) <= fmc1_la_p_b(26);
  adc_data_in_n(3)(6) <= fmc1_la_n_b(26);
  adc_data_in_p(3)(7) <= fmc1_la_p_b(27);
  adc_data_in_n(3)(7) <= fmc1_la_n_b(27);

  fmc_g : for i in 0 to 3 generate

    ibufds_i : ibufds
      port map (
        I  => adc_lclk_p(i),
        IB => adc_lclk_n(i),
        O  => adc_lclk(i));

    -- MMCME2_BASE_inst : MMCME2_BASE
    --   generic map (
    --     BANDWIDTH        => "OPTIMIZED",
    --     CLKFBOUT_MULT_F  => 4.0,
    --     CLKFBOUT_PHASE   => 0.0,
    --     CLKIN1_PERIOD    => 2.0,
    --     CLKOUT1_DIVIDE   => 8,
    --     CLKOUT0_DIVIDE_F => 2.0,
    --     DIVCLK_DIVIDE    => 2,
    --     REF_JITTER1      => 0.01,
    --     STARTUP_WAIT     => FALSE)
    --   port map (
    --     CLKOUT0  => mmcm_clkout0(i),
    --     CLKOUT1  => mmcm_clkout1(i),
    --     CLKFBOUT => mmcm_fbout(i),
    --     CLKIN1   => adc_lclk(i),
    --     PWRDWN   => '0',
    --     RST      => '0',
    --     CLKFBIN  => mmcm_fbin(i));

    -- bufgfb_i : bufg
    --   port map (
    --     I => mmcm_fbout(i),
    --     O => mmcm_fbin(i));

    -- bufio_i : bufg
    --   port map (
    --     I => mmcm_clkout0(i),
    --     O => adc_clk_bufio(i));

    -- bufg1_i : bufg
    --   port map (
    --     I => mmcm_clkout1(i),
    --     O => adc_clk_bufr(i));
    bufio_i : bufio
      port map (
        I => adc_lclk(i),
        O => adc_clk_bufio(i));

    bufr_i : bufr
      generic map (
        SIM_DEVICE  => "7SERIES",
        BUFR_DIVIDE => "4")
      port map (
        I   => adc_lclk(i),
        CE  => '1',
        CLR => '0',
        O   => adc_clk_bufr(i));

    process (adc_clk_bufr(i)) is
    begin
      if rising_edge(adc_clk_bufr(i)) then
      end if;
    end process;

    p_auto_bitslip : process (adc_clk_bufr(i))
    begin
      if rising_edge(adc_clk_bufr(i)) then
        -- Shift register to generate bitslip enable once every 8 clock ticks
        bitslip_sreg(i) <= bitslip_sreg(i)(0) & bitslip_sreg(i)(bitslip_sreg'length-1 downto 1);

        -- Generate bitslip and synced signal
        if(bitslip_sreg(i)(bitslip_sreg'LEFT) = '1') then
          if(unsigned(adc_fclk(i)) /= "11110000") then
            serdes_auto_bslip(i) <= '1';
          else
            serdes_auto_bslip(i) <= '0';
          end if;
        else
          serdes_auto_bslip(i) <= '0';
        end if;
      end if;
    end process p_auto_bitslip;

    adc_lane_fclk : entity work.adc_lane
      port map (
        adc_clk_bufio_i => adc_clk_bufio(i),
        adc_clk_bufr_i  => adc_clk_bufr(i),
        rst_i           => '0',
        bitslip_i       => serdes_auto_bslip(i),
        adc_data_p_i    => adc_fclk_p(i),
        adc_data_n_i    => adc_fclk_n(i),
        adc_data_o      => adc_fclk(i));

    adc_g : for j in 0 to 7 generate

      adc_lane_data : entity work.adc_lane
        port map (
          adc_clk_bufio_i => adc_clk_bufio(i),
          adc_clk_bufr_i  => adc_clk_bufr(i),
          rst_i           => '0',
          bitslip_i       => serdes_auto_bslip(i),
          adc_data_p_i    => adc_data_in_p(i)(j),
          adc_data_n_i    => adc_data_in_n(i)(j),
          adc_data_o      => adc_data(i, j));

      ila_sigs(64*i+8*(j+1)-1 downto 64*i+8*j) <= adc_data(i, j);

    end generate adc_g;

  end generate fmc_g;

  ila_clk <= adc_clk_bufr(0);

  ila_i : entity work.ila
    generic map (
      ila_en => TRUE)
    port map (
      clk0 => ila_clk,
      ila0 => ila_sigs);

end architecture;



