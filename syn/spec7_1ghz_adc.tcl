set projDir [file dirname [info script]]
set speed   1
set kintex7 xc7k160tfbg676-${speed}
set device ${kintex7}


read_vhdl   ../rtl/ila_syn.vhd
read_vhdl   ../rtl/adc_lane.vhd
read_vhdl   ../rtl/spec7_1ghz_adc.vhd
read_edif  ./chipscope_ila.ngc
read_edif  ./chipscope_icon.ngc

read_xdc ./spec7_1ghz_adc.xdc 

set start_time [clock seconds]

synth_design -top spec7_1ghz_adc -part $device  > post_synth.txt
#write_checkpoint -force ${projDir}/post_synth

#create_debug_core u_ila_0 ila
#set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_0]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
#set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
#set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
#set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
#set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
#set_property port_width 1 [get_debug_ports u_ila_0/clk]
#connect_debug_port u_ila_0/clk [get_nets [list vc709_clks_i/clk125_o ]]
#set_property port_width 128 [get_debug_ports u_ila_0/probe0]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
#connect_debug_port u_ila_0/probe0 [get_nets [list {ila_i/ila0_h[0]} {ila_i/ila0_h[1]} {ila_i/ila0_h[2]} {ila_i/ila0_h[3]} {ila_i/ila0_h[4]} {ila_i/ila0_h[5]} {ila_i/ila0_h[6]} {ila_i/ila0_h[7]} {ila_i/ila0_h[8]} {ila_i/ila0_h[9]} {ila_i/ila0_h[10]} {ila_i/ila0_h[11]} {ila_i/ila0_h[12]} {ila_i/ila0_h[13]} {ila_i/ila0_h[14]} {ila_i/ila0_h[15]} {ila_i/ila0_h[16]} {ila_i/ila0_h[17]} {ila_i/ila0_h[18]} {ila_i/ila0_h[19]} {ila_i/ila0_h[20]} {ila_i/ila0_h[21]} {ila_i/ila0_h[22]} {ila_i/ila0_h[23]} {ila_i/ila0_h[24]} {ila_i/ila0_h[25]} {ila_i/ila0_h[26]} {ila_i/ila0_h[27]} {ila_i/ila0_h[28]} {ila_i/ila0_h[29]} {ila_i/ila0_h[30]} {ila_i/ila0_h[31]} {ila_i/ila0_h[32]} {ila_i/ila0_h[33]} {ila_i/ila0_h[34]} {ila_i/ila0_h[35]} {ila_i/ila0_h[36]} {ila_i/ila0_h[37]} {ila_i/ila0_h[38]} {ila_i/ila0_h[39]} {ila_i/ila0_h[40]} {ila_i/ila0_h[41]} {ila_i/ila0_h[42]} {ila_i/ila0_h[43]} {ila_i/ila0_h[44]} {ila_i/ila0_h[45]} {ila_i/ila0_h[46]} {ila_i/ila0_h[47]} {ila_i/ila0_h[48]} {ila_i/ila0_h[49]} {ila_i/ila0_h[50]} {ila_i/ila0_h[51]} {ila_i/ila0_h[52]} {ila_i/ila0_h[53]} {ila_i/ila0_h[54]} {ila_i/ila0_h[55]} {ila_i/ila0_h[56]} {ila_i/ila0_h[57]} {ila_i/ila0_h[58]} {ila_i/ila0_h[59]} {ila_i/ila0_h[60]} {ila_i/ila0_h[61]} {ila_i/ila0_h[62]} {ila_i/ila0_h[63]} {ila_i/ila0_h[64]} {ila_i/ila0_h[65]} {ila_i/ila0_h[66]} {ila_i/ila0_h[67]} {ila_i/ila0_h[68]} {ila_i/ila0_h[69]} {ila_i/ila0_h[70]} {ila_i/ila0_h[71]} {ila_i/ila0_h[72]} {ila_i/ila0_h[73]} {ila_i/ila0_h[74]} {ila_i/ila0_h[75]} {ila_i/ila0_h[76]} {ila_i/ila0_h[77]} {ila_i/ila0_h[78]} {ila_i/ila0_h[79]} {ila_i/ila0_h[80]} {ila_i/ila0_h[81]} {ila_i/ila0_h[82]} {ila_i/ila0_h[83]} {ila_i/ila0_h[84]} {ila_i/ila0_h[85]} {ila_i/ila0_h[86]} {ila_i/ila0_h[87]} {ila_i/ila0_h[88]} {ila_i/ila0_h[89]} {ila_i/ila0_h[90]} {ila_i/ila0_h[91]} {ila_i/ila0_h[92]} {ila_i/ila0_h[93]} {ila_i/ila0_h[94]} {ila_i/ila0_h[95]} {ila_i/ila0_h[96]} {ila_i/ila0_h[97]} {ila_i/ila0_h[98]} {ila_i/ila0_h[99]} {ila_i/ila0_h[100]} {ila_i/ila0_h[101]} {ila_i/ila0_h[102]} {ila_i/ila0_h[103]} {ila_i/ila0_h[104]} {ila_i/ila0_h[105]} {ila_i/ila0_h[106]} {ila_i/ila0_h[107]} {ila_i/ila0_h[108]} {ila_i/ila0_h[109]} {ila_i/ila0_h[110]} {ila_i/ila0_h[111]} {ila_i/ila0_h[112]} {ila_i/ila0_h[113]} {ila_i/ila0_h[114]} {ila_i/ila0_h[115]} {ila_i/ila0_h[116]} {ila_i/ila0_h[117]} {ila_i/ila0_h[118]} {ila_i/ila0_h[119]} {ila_i/ila0_h[120]} {ila_i/ila0_h[121]} {ila_i/ila0_h[122]} {ila_i/ila0_h[123]} {ila_i/ila0_h[124]} {ila_i/ila0_h[125]} {ila_i/ila0_h[126]} {ila_i/ila0_h[127]} ]]


#opt_design -directive Explore  > post_opt.txt
place_design -directive Explore > post_place.txt
#write_checkpoint -force ${projDir}/post_place

#phys_opt_design -directive Explore > post_physopt.txt
#write_checkpoint -force ${projDir}/post_phys_opt

route_design -directive Explore > post_route.txt
#write_checkpoint -force ${projDir}/post_route

#report_timing_summary -file ${projDir}/timing_summary.rpt
#report_timing -sort_by group -max_paths 100 -path_type full -file ${projDir}/timing.rpt
#report_utilization -hierarchical -file ${projDir}/utilization.rpt
#report_io -file ${projDir}/pin.rpt

set_property CONFIG_VOLTAGE             1.8  [current_design]
set_property CFGBVS                     GND  [current_design]
set_property BITSTREAM.GENERAL.COMPRESS true [current_design]
write_bitstream -force ${projDir}/spec7_1ghz_adc.bit

set end_time [clock seconds]
set total_time [ expr { $end_time - $start_time} ]
set absolute_time [clock format $total_time -format {%H:%M:%S} -gmt true ]
puts "\ntotal build time: $absolute_time\n"
